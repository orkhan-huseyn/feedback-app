import { Suspense, useEffect } from 'react';
import { Route, Routes } from 'react-router-dom';
import HomePage from './pages/Home';
import RoadmapPage from './pages/Roadmap';
import CreateFeedbackPage from './pages/CreateFeedback';
import EditFeedbackPage from './pages/EditFeedback';
import FeedbackDetailsPage from './pages/FeedbackDetails';
import { useAppDispatch, useAppSelector } from './app/hooks';
import { fetchCurrentUser } from './features/user/thunks';

function App() {
  const dispatch = useAppDispatch();
  const userLoading = useAppSelector(state => state.user.loading);

  useEffect(() => {
    dispatch(fetchCurrentUser());
  }, []);

  if (userLoading) {
    return (
      <div className="h-full flex justify-center align-center">
        <h1>Authorizing...</h1>
      </div>
    );
  }

  return (
    <Suspense fallback={<h1>Loading page...</h1>}>
      <Routes>
        <Route index element={<HomePage />} />
        <Route path="/feedbacks/create" element={<CreateFeedbackPage />} />
        <Route path="/feedbacks/edit/:id" element={<EditFeedbackPage />} />
        <Route
          path="/feedbacks/defails/:id"
          element={<FeedbackDetailsPage />}
        />
        <Route path="/roadmap" element={<RoadmapPage />} />
      </Routes>
    </Suspense>
  );
}

export default App;
