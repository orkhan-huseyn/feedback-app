import { AxiosResponse } from 'axios';
import { Category, Feedback } from '../types';
import axios from './axios';

export function fetchFeedbacks(): Promise<AxiosResponse<Feedback[]>> {
  return axios.get('feedbacks');
}

export function insertFeedback(
  feedback: Feedback
): Promise<AxiosResponse<Feedback>> {
  return axios.post('feedbacks', feedback);
}

export function fetchCategories(): Promise<AxiosResponse<Category[]>> {
  return axios.get('categories');
}
