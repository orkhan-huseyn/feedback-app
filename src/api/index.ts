import * as userServices from './user';
import * as feedbackServices from './feedbacks';

const API = {
  ...userServices,
  ...feedbackServices,
};

export default API;
