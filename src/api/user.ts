import { AxiosResponse } from 'axios';
import { User } from '../types';
import axios from './axios';

export async function fetchUserById(
  userId: number
): Promise<AxiosResponse<User>> {
  return axios.get(`users/${userId}`);
}
