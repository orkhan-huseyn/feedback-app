import { Category } from '../types';

type CategorySelectedCallback = (category: Category) => void;

interface CategoryFiltersProps {
  categories: Category[];
  onCategorySelected: CategorySelectedCallback;
}

function CategoryFilters({
  categories,
  onCategorySelected,
}: CategoryFiltersProps) {

  
  function handleCategoryClick(category: Category) {
    onCategorySelected(category);
  }

  return (
    <div className="bg-white basis-1/3 rounder-lg p-2">
      {categories.map((category) => (
        <button
          key={category.id}
          onClick={() => handleCategoryClick(category)}
          className="bg-[#F2F4FF] text-[#4661e6] rounded-md p-2 mr-2"
        >
          {category.title}
        </button>
      ))}
    </div>
  );
}

export default CategoryFilters;
