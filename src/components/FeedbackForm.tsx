import { useRef } from 'react';
import { Link } from 'react-router-dom';
import { Category } from '../types';

interface FeedbackFormProps {
  onSubmit: (formData: any) => void;
  categories: Category[];
}

function FeedbackForm({ onSubmit, categories }: FeedbackFormProps) {
  const formRef = useRef<HTMLFormElement>(null);

  function handleFormSubmit(event: any) {
    event.preventDefault();
    const { title, details, category } = formRef.current?.elements as any;
    onSubmit({
      title: title.value,
      details: details.value,
      category: category.value,
    });
  }

  return (
    <form
      ref={formRef}
      onSubmit={handleFormSubmit}
      className="flex p-4 flex-col"
    >
      <input
        className="mt-5"
        type="text"
        name="title"
        placeholder="Feedback title..."
      />
      <textarea
        className="mt-5"
        placeholder="Details..."
        name="details"
        rows={5}
      ></textarea>
      <select name="category" defaultValue="none" className="mt-5">
        <option value="none" disabled>
          Select category
        </option>
        {categories.map((category) => (
          <option key={category.id} value={category.title}>
            {category.title}
          </option>
        ))}
      </select>
      <button>Submit</button>
      <Link to="/">Geri QAYIT!!!!</Link>
    </form>
  );
}

export default FeedbackForm;
