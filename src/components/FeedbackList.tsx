import { Link } from 'react-router-dom';
import { useAppDispatch } from '../app/hooks';
import { unVoteFeedback, voteFeedback } from '../features/feedbacks/slice';
import { Feedback } from '../types';
import VoteButton from './VoteButton';

interface FeedbackListProps {
  list: Feedback[];
  loading: boolean;
}

function FeedbackList({ list, loading }: FeedbackListProps) {
  const dispatch = useAppDispatch();

  function onVote(feedbackId: number) {
    dispatch(voteFeedback(feedbackId));
  }

  function onUnvote(feedbackId: number) {
    dispatch(unVoteFeedback(feedbackId));
  }

  return (
    <div className="flex flex-col basis-2/3 p-2">
      {list.map((feedback) => (
        <div key={feedback.id} className="flex rounded-sm bg-white p-4 mb-4">
          <div className="basis-1/6">
            <VoteButton
              onDecrement={() => onUnvote(feedback.id!)}
              onIncrement={() => onVote(feedback.id!)}
              count={feedback.voteCount}
            />
          </div>
          <div className="basis-5/6">
            <Link to={`/feedbacks/details/${feedback.id}`}>
              <h3 className="font-bold text-xl">{feedback.title}</h3>
            </Link>
            <p>{feedback.details}</p>
            <p>
              <span className="bg-[#F2F4FF] text-[#4661e6] rounded-md p-2 mr-2">
                {feedback.category}
              </span>
            </p>
          </div>
        </div>
      ))}
    </div>
  );
}

export default FeedbackList;
