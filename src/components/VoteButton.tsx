import { useState } from 'react';
import classNames from 'classnames';

interface VoteButtonProps {
  count: number;
  onIncrement: () => void;
  onDecrement: () => void;
  initiallyVoted?: boolean;
}

function VoteButton({
  count,
  onDecrement,
  onIncrement,
  initiallyVoted = false,
}: VoteButtonProps) {
  const [voted, setVoted] = useState<boolean>(initiallyVoted);

  const buttonClasses = classNames('rounded-xl p-3', {
    'bg-[#4661E6] text-white': voted,
    'bg-[#F2F4FE] text-[#3A4374]': !voted,
  });

  function handleClick() {
    if (voted) {
      onDecrement();
    } else {
      onIncrement();
    }
    setVoted((v) => !v);
  }

  return (
    <button onClick={handleClick} className={buttonClasses}>
      {count}
    </button>
  );
}

export default VoteButton;
