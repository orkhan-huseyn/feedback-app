import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { Feedback, Category } from '../../types';
import { fetchCategories, fetchFeedbacks, insertFeedback } from './thunks';

interface FeedbackState {
  feedbacks: Feedback[];
  categories: Category[];
  loading: boolean;
}

const initialState: FeedbackState = {
  feedbacks: [],
  categories: [],
  loading: true,
};

const feedbacksSlice = createSlice({
  name: 'feedbacks',
  initialState,
  reducers: {
    voteFeedback(state, action: PayloadAction<number>) {
      const feedback = state.feedbacks.find((f) => f.id === action.payload)!;
      feedback.voteCount++;
    },
    unVoteFeedback(state, action: PayloadAction<number>) {
      const feedback = state.feedbacks.find((f) => f.id === action.payload)!;
      feedback.voteCount--;
    },
  },
  extraReducers(builder) {
    builder.addCase(fetchCategories.fulfilled, (state, action) => {
      state.categories = action.payload;
      state.loading = false;
    });

    builder.addCase(fetchFeedbacks.fulfilled, (state, action) => {
      state.feedbacks = action.payload;
      state.loading = false;
    });

    builder.addCase(insertFeedback.fulfilled, (state, action) => {
      state.feedbacks.push(action.payload);
    });
  },
});

export const { voteFeedback, unVoteFeedback } = feedbacksSlice.actions;

export default feedbacksSlice.reducer;
