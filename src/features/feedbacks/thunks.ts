import { createAsyncThunk } from '@reduxjs/toolkit';
import API from '../../api';
import { Feedback } from '../../types';

export const fetchFeedbacks = createAsyncThunk(
  'feedbacks/fetch',
  async function () {
    return API.fetchFeedbacks().then((response) => response.data);
  }
);

export const insertFeedback = createAsyncThunk(
  'feedbacks/insert',
  async function (feedback: Feedback) {
    return API.insertFeedback(feedback).then((response) => response.data);
  }
);

export const fetchCategories = createAsyncThunk(
  'feedbacks/categories/fetch',
  async function () {
    return API.fetchCategories().then((response) => response.data);
  }
);
