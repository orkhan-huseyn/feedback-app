import { createSlice } from '@reduxjs/toolkit';
import { User } from '../../types';
import { fetchCurrentUser } from './thunks';

export interface UserState {
  sessionUser: User | null;
  loading: boolean;
}

const initialState: UserState = {
  sessionUser: null,
  loading: true,
};

export const userSlice = createSlice({
  name: 'user',
  initialState,
  reducers: {},
  extraReducers(builder) {
    builder.addCase(fetchCurrentUser.fulfilled, (state, action) => {
      state.sessionUser = action.payload;
      state.loading = false;
    });
  },
});

export default userSlice.reducer;
