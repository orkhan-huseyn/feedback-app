import { createAsyncThunk } from '@reduxjs/toolkit';
import { random } from '../../util/random';
import API from '../../api';

export const fetchCurrentUser = createAsyncThunk(
  'user/fetch',
  async function () {
    const id = random(1, 3);
    return API.fetchUserById(id).then((response) => response.data);
  }
);
