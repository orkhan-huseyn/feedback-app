import { useEffect } from 'react';
import { useAppDispatch, useAppSelector } from '../../app/hooks';
import FeedbackForm from '../../components/FeedbackForm';
import {
  fetchCategories,
  insertFeedback,
} from '../../features/feedbacks/thunks';

function CreateFeedback() {
  const dispatch = useAppDispatch();
  const { categories } = useAppSelector((state) => state.feedbacks);

  useEffect(() => {
    dispatch(fetchCategories());
  }, []);

  function onSubmit(formData: any) {
    const feedback = {
      ...formData,
      voteCount: 0,
      commentCount: 0,
    };
    dispatch(insertFeedback(feedback));
  }

  return (
    <div className="mt-10 max-w-6xl mx-auto">
      <FeedbackForm onSubmit={onSubmit} categories={categories} />
    </div>
  );
}

export default CreateFeedback;
