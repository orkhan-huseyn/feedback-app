import React from 'react';

const LazyCreateFeedbackPage = React.lazy(() => import('./CreateFeedback'));

export default LazyCreateFeedbackPage;
