import React from 'react';

const LazyEditFeedbackPage = React.lazy(() => import('./EditFeedback'));

export default LazyEditFeedbackPage;
