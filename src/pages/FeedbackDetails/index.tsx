import React from 'react';

const LazyFeedbackDetailsPage = React.lazy(() => import('./FeedbackDetails'));

export default LazyFeedbackDetailsPage;
