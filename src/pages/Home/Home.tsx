import { useEffect } from 'react';
import { Link } from 'react-router-dom';
import { useAppDispatch, useAppSelector } from '../../app/hooks';
import CategoryFilters from '../../components/CategoryFilter';
import FeedbackList from '../../components/FeedbackList';
import {
  fetchCategories,
  fetchFeedbacks,
} from '../../features/feedbacks/thunks';
import { Category } from '../../types';

function Home() {
  const dispatch = useAppDispatch();
  const { feedbacks, categories, loading } = useAppSelector(
    (state) => state.feedbacks
  );

  useEffect(() => {
    dispatch(fetchCategories());
    dispatch(fetchFeedbacks());
  }, []);

  function handleCategorySelect(category: Category) {
    console.log(category);
  }

  return (
    <>
      <Link className="bg-[#C75AF6] p-2 rounded-sm text-white" to="/feedbacks/create">
        +Add feedback
      </Link>
      <div className="flex mt-10 max-w-6xl mx-auto">
        <CategoryFilters
          onCategorySelected={handleCategorySelect}
          categories={categories}
        />
        <FeedbackList list={feedbacks} loading={loading} />
      </div>
    </>
  );
}

export default Home;
