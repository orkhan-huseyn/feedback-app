import React from 'react';

const LazyHomePage = React.lazy(() => import('./Home'));

export default LazyHomePage;
