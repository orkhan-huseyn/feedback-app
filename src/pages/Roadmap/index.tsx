import React from 'react';

const LazyRoadmapPage = React.lazy(() => import('./Roadmap'));

export default LazyRoadmapPage;
