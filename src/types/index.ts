export interface User {
  id: number;
  firstName: string;
  lastName: string;
  gender: string;
  email: string;
  image: string;
}

export type CategoryText = 'Feature' | 'UI' | 'UX' | 'Enhancement' | 'Bug';
export interface Category {
  id: number;
  title: CategoryText;
}

export interface Feedback {
  id?: number;
  title: string;
  details: string;
  category: CategoryText;
  voteCount: number;
  commentCount: number;
}
